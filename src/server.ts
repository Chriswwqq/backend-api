import Fastify, { FastifyInstance, RouteShorthandOptions } from "fastify";

const server: FastifyInstance = Fastify({ logger: true });

const opts: RouteShorthandOptions = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          pong: {
            type: "string",
          },
        },
      },
    },
  },
};

server.get("/", opts, async (request, reply) => {
  return { pong: "Change the response" };
});

const start = async () => {
  try {
    const port = process.env.PORT ? Number(process.env.PORT) : 3000;
    await server.listen(port);

    const address = server.server.address();
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

start();

export default server;
